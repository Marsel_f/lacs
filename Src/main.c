/**
  ******************************************************************************
  * File Name          : main.c
  * Description        : Main program body
  ******************************************************************************
  *
  * Copyright (c) 2017 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f4xx_hal.h"
#include "adc.h"
#include "dac.h"
#include "dma.h"
#include "fatfs.h"
#include "i2c.h"
#include "rtc.h"
#include "sdio.h"
#include "spi.h"
#include "gpio.h"

/* USER CODE BEGIN Includes */
#include "stdbool.h"
#include "MAX3010x.h"
#include "ssd1306.h"
#include "ADXL345.h"
/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void Error_Handler(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/
void readBatLevel(void);
void SDTXBufferFill(char* str);
FRESULT SDWrite(void);
FRESULT SDPrintWelcomeLines(void);
FRESULT SDCreateFile(void);
FRESULT SDFMount(void);
void SDTXBufferFillBlock(void);
FRESULT SDCreateNewFile(void);
/* USER CODE END PFP */

/* USER CODE BEGIN 0 */
//uint8_t txdat;
#define PPG_SMPLS_NUM 1
#define MODE	2
/*uint8_t MAX3010xSamplesPacket = PPG_SMPLS_NUM;*/
uint8_t PPGData[PPG_SMPLS_NUM*MODE*3];

//uint8_t PPGData[127];
char buf[127]; 
uint8_t step;

uint16_t GSR_PREOUT, GSR_OUT;
#define GSR_OUT_LOW_THR		50
#define GSR_OUT_HIGH_THR	4045

uint16_t DACVal;
int16_t ADXL345Data[3];
uint16_t batteryADCConvertedVal;

uint32_t buttonPushMoment;
uint32_t buttonPullMoment;
#define BTN_		300
#define BTN__		500

FATFS SDFatFs;           /* File system object for USB disk logical drive */
FIL file;                   /* File object */
char SDPath[4];          /* USB Host logical drive path */
FRESULT res;                                          /* FatFs function common result code */
uint32_t byteswritten, bytesread;                     /* File write/read counts */
char fileName[] = "Test_00-00-00_00-00-00.txt";
//char bu[160];
//uint8_t chNum;
uint32_t errFlag = 0;

#define SD_TX_BUFF_SIZE	32768
//#define SD_TX_BUFF_TRANSMIT_SIZE	1024
uint8_t SDTXBuf1[SD_TX_BUFF_SIZE];
uint8_t SDTXBuf2[SD_TX_BUFF_SIZE];

//#define INITIAL_BLOCK 3317746

//uint32_t blockAddress[BLOCKSIZE*sizeof(uint8_t)/sizeof(uint32_t)] = {INITIAL_BLOCK};

bool currBufFlag;
uint32_t ptr = 0;
bool writeFlag = false;
uint32_t lastSaveTime;


uint32_t counti;

uint8_t adcCount = 0;

uint16_t ECG;
bool ADXL345ReadDataFlag = false;
uint16_t subsecond;
#define STR_MAX	20
uint8_t stringCount = 0;
//uint8_t OLEDUpdateAllowFlag = OLED_UPD_ALLOW;

uint8_t adc1step = 0, adc2step = 0;
/* USER CODE END 0 */

int main(void)
{

  /* USER CODE BEGIN 1 */
	RTC_TimeTypeDef sTime;
	RTC_DateTypeDef sDate;
  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* Configure the system clock */
  SystemClock_Config();

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_ADC2_Init();
  MX_RTC_Init();
  MX_SPI2_Init();
  MX_I2C2_Init();
  MX_I2C1_Init();
  MX_SDIO_SD_Init();
  MX_FATFS_Init();
  MX_ADC1_Init();
  MX_DAC_Init();

  /* USER CODE BEGIN 2 */
	HAL_Delay(1000);
	SSD1306_Init();
	char str[] = "Welcome!";
	//SSD1306_Fill(1);
	SSD1306_Puts(str, &Font_7x10, 1);
	SSD1306_UpdateScreen();
	counti = uwTick;
	HAL_Delay(100);

	ADXL345Init();
	
	SDFMount();
	SDCreateFile();	
	//MAX30102Init();
	//HAL_Delay(1000);
	
	/*GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.Pin = GPIO_PIN_5;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);*/
	//HAL_GPIO_EXTI_Callback(GPIO_PIN_5);
	HAL_NVIC_EnableIRQ(RTC_Alarm_IRQn);
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
		if(ADXL345ReadDataFlag == true) {
			ADXL345ReadDataFlag = false;
			ADXL345ReadData();
		}
		if(writeFlag == true)
		{
			writeFlag = false;
			SDWrite();
		}
  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */

  }
  /* USER CODE END 3 */

}

/** System Clock Configuration
*/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInitStruct;

    /**Configure the main internal regulator output voltage 
    */
  __HAL_RCC_PWR_CLK_ENABLE();

  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_LSE;
  RCC_OscInitStruct.LSEState = RCC_LSE_ON;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 168;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 7;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
  {
    Error_Handler();
  }

  PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_RTC;
  PeriphClkInitStruct.RTCClockSelection = RCC_RTCCLKSOURCE_LSE;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* USER CODE BEGIN 4 */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	uint8_t txdat;
	/*if(GPIO_Pin == GPIO_PIN_5)
	{
		txdat = 0x07;
		//ADXL345ReadDataFlag = true;
		HAL_ADC_Start_IT(&hadc1);
		//HAL_I2C_Master_Transmit_IT(&hi2c1, MAX30105_ADDRESS<<1, &txdat, 1);
		while(HAL_I2C_Master_Transmit_DMA(&hi2c1, MAX30105_ADDRESS<<1, &txdat, 1))
		{
			//if (HAL_I2C_GetError(&hi2c1) != HAL_I2C_ERROR_AF)
			//{
			//  Error_Handler();
			//}
		}
		step = 0;
	}
	else */if(GPIO_Pin == GPIO_PIN_6)
	{
		if(HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_6) == GPIO_PIN_SET)
		{
			buttonPushMoment = uwTick;
			//HAL_GPIO_WritePin(GPIOC, GPIO_PIN_2, GPIO_PIN_RESET);
			//HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_7);
		}
		if(HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_6) == GPIO_PIN_RESET)
		{
			buttonPullMoment = uwTick;
			if(buttonPullMoment < buttonPushMoment + BTN_)
			{
				//HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_7);
			}
			else if(buttonPullMoment > buttonPushMoment + BTN__)
			{
				HAL_GPIO_WritePin(GPIOC, GPIO_PIN_2, GPIO_PIN_RESET);
			}
		}
	}
}
void HAL_I2C_MasterTxCpltCallback(I2C_HandleTypeDef *hi2c)
{
	uint8_t rxdat;
	if(hi2c == &hi2c1)
	{
		if(step == 0)
		{
			//HAL_I2C_Master_Receive_IT(&hi2c1, MAX30105_ADDRESS<<1, &PPGData[0], 72);
			while(HAL_I2C_Master_Receive_DMA(&hi2c1, MAX30105_ADDRESS<<1, &PPGData[0], PPG_SMPLS_NUM*MODE*3) != HAL_OK)
			{
				//if (HAL_I2C_GetError(&hi2c1) != HAL_I2C_ERROR_AF)
				//{
				//	Error_Handler();
				//}   
			}
			step = 1;
		}
		else if(step == 2)
		{
			//HAL_I2C_Master_Receive_IT(&hi2c1, MAX30105_ADDRESS<<1, &PPGData[0], 1);
			while(HAL_I2C_Master_Receive_DMA(&hi2c1, MAX30105_ADDRESS<<1, &rxdat, 1) != HAL_OK)
			{
				//if (HAL_I2C_GetError(&hi2c1) != HAL_I2C_ERROR_AF)
				//{
				//	Error_Handler();
				//}   
			}
			step = 3;
			SDTXBufferFillBlock();
		}
	}
}
void HAL_I2C_MasterRxCpltCallback(I2C_HandleTypeDef *hi2c)
{
	uint8_t txdat;
	if(hi2c == &hi2c1)
	{
		if(step == 1)
		{
			txdat = 0x00;
			//HAL_I2C_Master_Transmit_IT(&hi2c1, MAX30105_ADDRESS<<1, &txdat, 1);
			while(HAL_I2C_Master_Transmit_DMA(&hi2c1, MAX30105_ADDRESS<<1, &txdat, 1))
			{
				//if (HAL_I2C_GetError(&hi2c1) != HAL_I2C_ERROR_AF)
				//{
				//	Error_Handler();
				//}
			}
			step = 2;
		}
	}
}

void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
{
	/*if(hadc == &hadc1 && adcCount == 0) {
		ECG = HAL_ADC_GetValue(&hadc1);
	}*/
	if(hadc == &hadc2) {
		ECG = HAL_ADC_GetValue(&hadc2);
	}
	else if (hadc == &hadc1 && adc1step == 1) {
		GSR_PREOUT = HAL_ADC_GetValue(&hadc1);
		adc1step = 2;
		HAL_ADC_Start_IT(&hadc1);
		
	}
	else if (hadc == &hadc1 && adc1step == 2) {
		GSR_OUT = HAL_ADC_GetValue(&hadc1);
		adc1step = 3;
	}
}
void SDTXBufferFill(char* str)
{
	while( *str ){
		if(currBufFlag)
			SDTXBuf1[ptr] = *str++;
		else
			SDTXBuf2[ptr] = *str++;
		ptr++; 
		if(ptr == SD_TX_BUFF_SIZE){
			currBufFlag = !currBufFlag; // Change buffer
			writeFlag = true;
			ptr = 0;
		}
	}
}
FRESULT SDWrite(void)
{
	uint8_t i = 10;
	uint16_t part;
	//HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7, GPIO_PIN_SET);
	do {
		if(!currBufFlag) {
			res = f_write(&file, SDTXBuf1, SD_TX_BUFF_SIZE, (void *)&byteswritten);
		}
		else {
			res = f_write(&file, SDTXBuf2, SD_TX_BUFF_SIZE, (void *)&byteswritten);
		}
		if((byteswritten != 0) && (res == FR_OK)) {
			i = 1;
		}
		i--;
	}	while(i>0);
	//HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7, GPIO_PIN_RESET);
	if((byteswritten == 0) || (res != FR_OK)) {
		Error_Handler();
	}
	else {
		//HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7, GPIO_PIN_SET);
		i = 10;
		do {
			res = f_sync(&file);
			if(res == FR_OK) {
				i = 1;
			}
			i--;
		} while(i > 0);
		if(res != FR_OK) {
			Error_Handler();
		}
		//HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7, GPIO_PIN_RESET);
	}
	return res;
}
FRESULT SDPrintWelcomeLines(void)
{
	char str[] = "#Hours	Minutes	Seconds	Subseconds	ECG	RedPPG	IRPPG	AxelX	AxelY	AxelZ\n";
	uint8_t i = 10;
	do {
		res = f_write(&file, str, (sizeof str) - 1, (void *)&byteswritten);
		if(res == FR_OK) {
			return res;
		} 
		i--;
	}	while(i>0);
	if(res != FR_OK) {
		//Error_Handler();
	}
	return res;
}
FRESULT SDCreateNewFile(void)
{
	uint8_t i = 10;
	do {
		res = f_close(&file);
		if(res == FR_OK) {
			i = 1;
		}
		i--;
	} while(i > 0);
		if(res != FR_OK) {
			//Error_Handler();
		}
	res	= SDCreateFile();
	return res;
}
FRESULT SDCreateFile(void)
{
	RTC_TimeTypeDef sTime;
	RTC_DateTypeDef sDate;
	uint8_t i = 10;
	
	HAL_RTC_GetTime(&hrtc, &sTime, FORMAT_BIN);
	HAL_RTC_GetDate(&hrtc, &sDate, FORMAT_BIN);
	sprintf(fileName, "Test_%02d-%02d-%02d_%02d-%02d-%02d.txt", 
	sDate.Year, sDate.Month, sDate.Date, sTime.Hours, sTime.Minutes, sTime.Seconds);
	
	do {
		res = f_open(&file, fileName, FA_CREATE_ALWAYS | FA_WRITE);
		if(res == FR_OK) {
			i = 1;
		} 
		i--;
	}	while(i>0);
	if(res != FR_OK) {
		//Error_Handler();
	}
	else {
		//SDPrintWelcomeLines();
		i = 10;
		do {
			res = f_sync(&file);
			if(res == FR_OK) {
				i=1;
			} 
			i--;
		}	while(i>0);	
	}
	if(res != FR_OK) {
		//Error_Handler();
	}
	lastSaveTime = sTime.Hours;
	return res;
}
FRESULT SDFMount(void)
{
	uint8_t i = 10;
	do {
		res = f_mount(&SDFatFs, (TCHAR const*)SDPath, 1);
		if(res == FR_OK)
		{
			return res;
		} 
		i--;
	}	while(i>0);
	
	if(res != FR_OK) {
		//Error_Handler();
	}
	return res;
}
void SDTXBufferFillBlock(void)
{
	RTC_TimeTypeDef sTime;
	
	
	
	char buf[100];
	if(stringCount) {
		sprintf(buf, "%04d\n",
			//sTime.SecondFraction - sTime.SubSeconds,
			ECG
		);
		stringCount--;
	}
	else {
		HAL_RTC_GetTime(&hrtc, &sTime, FORMAT_BIN);
		sprintf(buf, "%04d\t%04d\t%04d\t%04d\t%02d\t%02d\t%02d\t% 05d\t% 05d\t% 05d\n",
			
			ECG,
			GSR_PREOUT, 
			GSR_OUT,
			DACVal,
			sTime.Hours, 
			sTime.Minutes, 
			sTime.Seconds, 
			//sTime.SecondFraction - sTime.SubSeconds,
			ADXL345Data[0], 
			ADXL345Data[1], 
			ADXL345Data[2]
		);
		
		stringCount = STR_MAX - 1;
	}	
	ADXL345ReadDataFlag = true;//ADXL345ReadData();
	SDTXBufferFill(buf);
	
}


//#define RTC_C_MAX 2048
//uint32_t rtcCount = RTC_C_MAX;

void HAL_RTC_AlarmAEventCallback(RTC_HandleTypeDef * hrtc)
{
	adc2step = 1;
	HAL_ADC_Start_IT(&hadc2);
	
	adc1step = 1;
	HAL_ADC_Start_IT(&hadc1);
	if(GSR_OUT < GSR_OUT_LOW_THR || GSR_OUT > GSR_OUT_HIGH_THR)
	{
		HAL_DAC_SetValue(&hdac, DAC_CHANNEL_1, DAC_ALIGN_12B_R, GSR_PREOUT);
		HAL_DAC_Start(&hdac, DAC_CHANNEL_1);
	}
	DACVal = HAL_DAC_GetValue(&hdac, DAC_CHANNEL_1);
	SDTXBufferFillBlock();
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler */
  /* User can add his own implementation to report the HAL error return state */
	//char str[] = "Herrrr";
	char buffer[10];
	errFlag++;
	if(uwTick - counti > 1000)
	{
		counti = uwTick;
		sprintf(buffer, "%d %d", res, errFlag);
		SSD1306_Fill(SSD1306_COLOR_BLACK);
		SSD1306_GotoXY(0, 0);	
		SSD1306_Puts(buffer, &Font_7x10, 1);
		SSD1306_UpdateScreen();	
		
	}
	/*f_close(&file);
	if(res != FR_OK) {
		res = f_close(&file);
	}*/
	f_mount(NULL, (TCHAR const*)SDPath, 1);
	
	HAL_SD_DeInit(&hsd);
	HAL_SD_Init(&hsd);
	HAL_SD_InitCard(&hsd);
	
	
	f_mount(&SDFatFs, (TCHAR const*)SDPath, 1);
	
	/*RTC_TimeTypeDef sTime;
	RTC_DateTypeDef sDate;
	
	HAL_RTC_GetTime(&hrtc, &sTime, FORMAT_BIN);
	HAL_RTC_GetDate(&hrtc, &sDate, FORMAT_BIN);
	sprintf(fileName, "Test_%04d-%02d-%02d_%02d-%02d-%02d.txt", 
	sDate.Year, sDate.Month, sDate.Date, sTime.Hours, sTime.Minutes, sTime.Seconds);*/
	
	res = f_open(&file, fileName, FA_OPEN_EXISTING | FA_WRITE);
	if(res == FR_OK) {
		res = f_lseek(&file, f_size(&file));
	}
	else {
		res = f_open(&file, fileName, FA_CREATE_ALWAYS | FA_WRITE);
		SDPrintWelcomeLines();
		f_sync(&file);
	}
	
	//lastSaveTime = sTime.Hours;
	

	//HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7, GPIO_PIN_SET);
  /* USER CODE END Error_Handler */ 
}

#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
