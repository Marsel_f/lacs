#ifndef ADXL345_H
#define ADXL345_H
#include "stm32f4xx.h"
//#include "stdint.h"

extern SPI_HandleTypeDef hspi2;
extern int16_t ADXL345Data[3];
void ADXL345Init(void);
void ADXL345ReadData(void);	
void ADXL345OffsetCalibration(void);
#endif //ADXL345_H
