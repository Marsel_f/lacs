/*************************************************** 
 This is a library written for the Maxim MAX30105 Optical Smoke Detector
 It should also work with the MAX30102. However, the MAX30102 does not have a Green LED.

 These sensors use I2C to communicate, as well as a single (optional)
 interrupt line that is not currently supported in this driver.
 
 Written by Peter Jansen and Nathan Seidle (SparkFun)
 BSD license, all text above must be included in any redistribution.
 *****************************************************/

#ifndef MAX3010X_H
#define MAX3010X_H

#include "stdbool.h"
//#include "i2c.h"

/*typedef enum 
{
  DISABLE = 0, 
  ENABLE = !DISABLE
} FuncState;*/
typedef bool FuncState;

//#define MAX3010X_INT_ENABLED 
#include "stdint.h"
#include "stm32f4xx_hal.h"
extern uint8_t MAX3010xSamplesPacket;
extern I2C_HandleTypeDef hi2c1;
extern I2C_HandleTypeDef hi2c2;
#define MAX3010X_I2C I2C1

#define MAX30105_ADDRESS          0x57 //7-bit I2C Address
//Note that MAX30102 has the same I2C address and Part ID

/*#define I2C_SPEED_STANDARD        100000
#define I2C_SPEED_FAST            400000
*/
typedef enum
{
	MAX30101 = 1,
	MAX30102 = 2,
	MAX30105 = 5
} MAXModel;
#define FIFOAFULLEMPTYSAMPLES 16
extern uint32_t FIFO[FIFOAFULLEMPTYSAMPLES];
//activeLEDs is the number of channels turned on, and can be 1 to 3. 2 is common for Red+IR.
//uint8_t activeLEDs; //Gets set during setup. Allows check() to calculate how many bytes to read from FIFO

//uint8_t revisionID; 

typedef struct
{
	MAXModel MaxModel;
	uint8_t LedMode;
  uint8_t RedLEDCurrent;
	uint8_t IRLEDCurrent;
	uint8_t GreenLEDCurrent;
	uint8_t ProximityModeLEDCurrent;
	uint8_t AveragedSamplesNum;
	uint16_t SampleRate;
	uint16_t PulseWidth;
	uint16_t ADCRange;
	FunctionalState FIFORollover;
	FunctionalState AFULLInterrupt;
	FunctionalState DataReadyInterrupt;
	FunctionalState ALCOverflowInterrupt;
	uint8_t FIFOAFullEmptySamples;
	uint8_t FIFOAFullFilledSamples;
	//uint8_t SlotsNum;
}MAX3010xInitTypeDef;

uint8_t getREG(uint8_t regNum);
//boolean begin(TwoWire &wirePort = Wire, uint32_t i2cSpeed = I2C_SPEED_STANDARD, uint8_t i2caddr = MAX30105_ADDRESS);

uint32_t getRed(void); //Returns immediate red value
uint32_t getIR(void); //Returns immediate IR value
uint32_t getGreen(void); //Returns immediate green value
//bool safeCheck(uint8_t maxTimeToCheck); //Given a max amount of time, check for new data

// Configuration
void softReset(void);
void shutDown(void); 
void wakeUp(void); 

void setLEDMode(uint8_t mode);

void setADCRange(uint8_t adcRange);
void setSampleRate(uint8_t sampleRate);
void setPulseWidth(uint8_t pulseWidth);

void setPulseAmplitudeRed(uint8_t value);
void setPulseAmplitudeIR(uint8_t value);
void setPulseAmplitudeGreen(uint8_t value);
void setPulseAmplitudeProximity(uint8_t value);

void setProximityThreshold(uint8_t threshMSB);

//Multi-led configuration mode (page 22)
void enableSlot(uint8_t slotNumber, uint8_t device); //Given slot number, assign a device to slot
void disableSlots(void);

// Data Collection

//Interrupts (page 13, 14)
uint8_t getINT1(void); //Returns the main interrupt group
uint8_t getINT2(void); //Returns the temp ready interrupt
void enableAFULL(void); //Enable/disable individual interrupts
void disableAFULL(void);
void enableDATARDY(void);
void disableDATARDY(void);
void enableALCOVF(void);
void disableALCOVF(void);
void enablePROXINT(void);
void disablePROXINT(void);
void enableDIETEMPRDY(void);
void disableDIETEMPRDY(void);

//FIFO Configuration (page 18)
void setFIFOAverage(uint8_t samples);
void enableFIFORollover(void);
void disableFIFORollover(void);
void setFIFOAlmostFull(uint8_t samples);

//FIFO Reading
uint16_t check(void); //Checks for new data and fills FIFO
uint8_t available(void); //Tells caller how many new samples are available (head - tail)
void nextSample(void); //Advances the tail of the sense array
uint32_t getFIFORed(void); //Returns the FIFO sample pointed to by tail
uint32_t getFIFOIR(void); //Returns the FIFO sample pointed to by tail
uint32_t getFIFOGreen(void); //Returns the FIFO sample pointed to by tail

uint8_t getWritePointer(void);
uint8_t getReadPointer(void);
void clearFIFO(void); //Sets the read/write pointers to zero

//Proximity Mode Interrupt Threshold
void setPROXINTTHRESH(uint8_t val);

// Die Temperature
float readTemperature(void);
float readTemperatureF(void);

// Detecting ID/Revision
uint8_t getRevisionID(void);
uint8_t readPartID(void);  


// Low-level I2C communication
extern uint8_t readRegister8(uint8_t reg);
extern void writeRegister8(uint8_t reg, uint8_t value);
extern uint32_t readRegister8sh(uint8_t reg);
//TwoWire *_i2cPort; //The generic connection to user's chosen I2C hardware
//uint8_t _i2caddr;

void readRevisionID(void);

void bitMask(uint8_t reg, uint8_t mask, uint8_t thing);
//**********************************
//**********************************
//**********************************
void MAX30102Init(void);
void MAX3010x_Init(MAX3010xInitTypeDef* MAX3010xInitStruct);
uint8_t MAX3010x_Test(void);
void setReadPointer(uint8_t);
uint32_t readRegister8sh(uint8_t reg);
uint32_t MAX3010x_GetLastRedVal(void);
uint8_t MAX3010xReadRegister(uint8_t reg);
uint8_t MAX3010xWriteRegister(uint8_t reg, uint8_t value);
	//**********************************
//**********************************
//**********************************
	
#endif //MAX3020X_H
