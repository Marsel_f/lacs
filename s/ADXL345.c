#include "stm32f4xx.h"
#include "ADXL345.h"

void ADXL345Init(void)
{
	uint8_t initTxData[] = {0x31, 0x0B, 0x2c, 0x0d, 0x2d, 0x08};
	uint8_t initRxData[sizeof initTxData];
	
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12, GPIO_PIN_RESET);
	HAL_SPI_TransmitReceive(&hspi2, initTxData, initRxData, sizeof initTxData, 100);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12, GPIO_PIN_SET);
}

void ADXL345ReadData(void)
{
	uint8_t temp[7];
	uint8_t TxData[7] = {0xf2};

	HAL_Delay(15);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12, GPIO_PIN_RESET);
	HAL_SPI_TransmitReceive(&hspi2, TxData, temp, 7, 100);
	//HAL_SPI_Transmit(&hspi2, TxData, 1, 100);
	//HAL_SPI_Transmit(&hspi2, temp, 6, 100);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12, GPIO_PIN_SET);
	
	ADXL345Data[0] = (int16_t)(temp[1]|(temp[2]<<8));
	ADXL345Data[1] = (int16_t)(temp[3]|(temp[4]<<8));
	ADXL345Data[2] = (int16_t)(temp[5]|(temp[6]<<8))/*-19*/;
}
void ADXL345OffsetCalibration(void)
{
	uint8_t i;
	uint8_t offset[6]={0x1e, 0, 0x1f, 0, 0x20, 0};
	uint8_t offset2[6];
	int32_t temp[3] = {0, 0, 0};
	
	/*for(i=0; i<100; i++)
	{
		ADXL345ReadData();
		temp[0] += ADXL345Data[0];
		temp[1] += ADXL345Data[1];
		temp[2] += ADXL345Data[2];
	}
	offset[1] = (uint8_t)(-(temp[0]>>2));
	offset[3] = (uint8_t)(-(temp[1]>>2));
	offset[5] = (uint8_t)(-((temp[2] - 256)>>2));*/
	/*offset[1] = (uint8_t)(-0);
	offset[3] = (uint8_t)(-0);
	offset[5] = (uint8_t)(255);*/
	
	HAL_SPI_TransmitReceive(&hspi2, offset, offset2, 6, 100);
}

