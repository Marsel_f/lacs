/***************************************************
  This is a library written for the Maxim MAX30105 Optical Smoke Detector
  It should also work with the MAX30102. However, the MAX30102 does not have a Green LED.

  These sensors use I2C to communicate, as well as a single (optional)
  interrupt line that is not currently supported in this driver.

  Written by Peter Jansen and Nathan Seidle (SparkFun)
  BSD license, all text above must be included in any redistribution.
 *****************************************************/
#include "stdint.h"
#include "MAX3010x.h"

//Bin to hex
#define LongToBin(n) \
(\
((n >> 21) & 0x80) | \
((n >> 18) & 0x40) | \
((n >> 15) & 0x20) | \
((n >> 12) & 0x10) | \
((n >>  9) & 0x08) | \
((n >>  6) & 0x04) | \
((n >>  3) & 0x02) | \
((n      ) & 0x01)   \
)
#define bin(n) LongToBin(0x##n##l)

#ifndef ssd1306_I2C_TIMEOUT
#define ssd1306_I2C_TIMEOUT					20000
#endif
static uint32_t ssd1306_I2C_Timeout;
#define TIMEOUT 20000
uint32_t Timeout = TIMEOUT;
// Status Registers
static const uint8_t MAX30105_INTSTAT1 =		0x00;
static const uint8_t MAX30105_INTSTAT2 =		0x01;
static const uint8_t MAX30105_INTENABLE1 =		0x02;
static const uint8_t MAX30105_INTENABLE2 =		0x03;

// FIFO Registers
static const uint8_t MAX30105_FIFOWRITEPTR = 	0x04;
static const uint8_t MAX30105_FIFOOVERFLOW = 	0x05;
static const uint8_t MAX30105_FIFOREADPTR = 	0x06;
static const uint8_t MAX30105_FIFODATA =		0x07;

// Configuration Registers
static const uint8_t MAX30105_FIFOCONFIG = 		0x08;
static const uint8_t MAX30105_MODECONFIG = 		0x09;
static const uint8_t MAX30105_PARTICLECONFIG = 	0x0A;    // Note, sometimes listed as "SPO2" config in datasheet (pg. 11)
static const uint8_t MAX30105_LED1_PULSEAMP = 	0x0C;
static const uint8_t MAX30105_LED2_PULSEAMP = 	0x0D;
static const uint8_t MAX30105_LED3_PULSEAMP = 	0x0E;
static const uint8_t MAX30105_LED_PROX_AMP = 	0x10;
static const uint8_t MAX30105_MULTILEDCONFIG1 = 0x11;
static const uint8_t MAX30105_MULTILEDCONFIG2 = 0x12;

// Die Temperature Registers
static const uint8_t MAX30105_DIETEMPINT = 		0x1F;
static const uint8_t MAX30105_DIETEMPFRAC = 	0x20;
static const uint8_t MAX30105_DIETEMPCONFIG = 	0x21;

// Proximity Function Registers
static const uint8_t MAX30105_PROXINTTHRESH = 	0x30;

// Part ID Registers
static const uint8_t MAX30105_REVISIONID = 		0xFE;
static const uint8_t MAX30105_PARTID = 			0xFF;    // Should always be 0x15. Identical to MAX30102.
//**********************************
//**********************************
//**********************************
static const uint8_t MAX30105_FIFOREADPTR_MASK = 	bin(00011111);
static const uint8_t MAX30105_FIFODEPTH = 	32;
//static const FuncState DISABLE	= false;
//static const FuncState ENABLE = true;
//**********************************
//**********************************
//**********************************
// MAX30105 Commands
// Interrupt configuration (pg 13, 14)
static const uint8_t MAX30105_INT_A_FULL_MASK =		(uint8_t)~bin(10000000);
static const uint8_t MAX30105_INT_A_FULL_ENABLE = 	0x80;
static const uint8_t MAX30105_INT_A_FULL_DISABLE = 	0x00;

static const uint8_t MAX30105_INT_DATA_RDY_MASK = (uint8_t)~bin(01000000);
static const uint8_t MAX30105_INT_DATA_RDY_ENABLE =	0x40;
static const uint8_t MAX30105_INT_DATA_RDY_DISABLE = 0x00;

static const uint8_t MAX30105_INT_ALC_OVF_MASK = (uint8_t)~bin(00100000);
static const uint8_t MAX30105_INT_ALC_OVF_ENABLE = 	0x20;
static const uint8_t MAX30105_INT_ALC_OVF_DISABLE = 0x00;

static const uint8_t MAX30105_INT_PROX_INT_MASK = (uint8_t)~bin(00010000);
static const uint8_t MAX30105_INT_PROX_INT_ENABLE = 0x10;
static const uint8_t MAX30105_INT_PROX_INT_DISABLE = 0x00;

static const uint8_t MAX30105_INT_DIE_TEMP_RDY_MASK = (uint8_t)~bin(00000010);
static const uint8_t MAX30105_INT_DIE_TEMP_RDY_ENABLE = 0x02;
static const uint8_t MAX30105_INT_DIE_TEMP_RDY_DISABLE = 0x00;

static const uint8_t MAX30105_SAMPLEAVG_MASK =	(uint8_t)~bin(11100000);
static const uint8_t MAX30105_SAMPLEAVG_1 = 	0x00;
static const uint8_t MAX30105_SAMPLEAVG_2 = 	0x20;
static const uint8_t MAX30105_SAMPLEAVG_4 = 	0x40;
static const uint8_t MAX30105_SAMPLEAVG_8 = 	0x60;
static const uint8_t MAX30105_SAMPLEAVG_16 = 	0x80;
static const uint8_t MAX30105_SAMPLEAVG_32 = 	0xA0;

static const uint8_t MAX30105_ROLLOVER_MASK = 	0xEF;
static const uint8_t MAX30105_ROLLOVER_ENABLE = 0x10;
static const uint8_t MAX30105_ROLLOVER_DISABLE = 0x00;

static const uint8_t MAX30105_A_FULL_MASK = 	0xF0;

// Mode configuration commands (page 19)
static const uint8_t MAX30105_SHUTDOWN_MASK = 	0x7F;
static const uint8_t MAX30105_SHUTDOWN = 		0x80;
static const uint8_t MAX30105_WAKEUP = 			0x00;

static const uint8_t MAX30105_RESET_MASK = 		0xBF;
static const uint8_t MAX30105_RESET = 			0x40;

static const uint8_t MAX30105_MODE_MASK = 		0xF8;
static const uint8_t MAX30105_MODE_REDONLY = 	0x02;
static const uint8_t MAX30105_MODE_REDIRONLY = 	0x03;
static const uint8_t MAX30105_MODE_MULTILED = 	0x07;

// Particle sensing configuration commands (pgs 19-20)
static const uint8_t MAX30105_ADCRANGE_MASK = 	0x9F;
static const uint8_t MAX30105_ADCRANGE_2048 = 	0x00;
static const uint8_t MAX30105_ADCRANGE_4096 = 	0x20;
static const uint8_t MAX30105_ADCRANGE_8192 = 	0x40;
static const uint8_t MAX30105_ADCRANGE_16384 = 	0x60;

static const uint8_t MAX30105_SAMPLERATE_MASK = 0xE3;
static const uint8_t MAX30105_SAMPLERATE_50 = 	0x00;
static const uint8_t MAX30105_SAMPLERATE_100 = 	0x04;
static const uint8_t MAX30105_SAMPLERATE_200 = 	0x08;
static const uint8_t MAX30105_SAMPLERATE_400 = 	0x0C;
static const uint8_t MAX30105_SAMPLERATE_800 = 	0x10;
static const uint8_t MAX30105_SAMPLERATE_1000 = 0x14;
static const uint8_t MAX30105_SAMPLERATE_1600 = 0x18;
static const uint8_t MAX30105_SAMPLERATE_3200 = 0x1C;

static const uint8_t MAX30105_PULSEWIDTH_MASK = 0xFC;
static const uint8_t MAX30105_PULSEWIDTH_69 = 	0x00;
static const uint8_t MAX30105_PULSEWIDTH_118 = 	0x01;
static const uint8_t MAX30105_PULSEWIDTH_215 = 	0x02;
static const uint8_t MAX30105_PULSEWIDTH_411 = 	0x03;

//Multi-LED Mode configuration (pg 22)
static const uint8_t MAX30105_SLOT1_MASK = 		0xF8;
static const uint8_t MAX30105_SLOT2_MASK = 		0x8F;
static const uint8_t MAX30105_SLOT3_MASK = 		0xF8;
static const uint8_t MAX30105_SLOT4_MASK = 		0x8F;

static const uint8_t SLOT_NONE = 				0x00;
static const uint8_t SLOT_RED_LED = 			0x01;
static const uint8_t SLOT_IR_LED = 				0x02;
static const uint8_t SLOT_GREEN_LED = 			0x03;
static const uint8_t SLOT_NONE_PILOT = 			0x04;
static const uint8_t SLOT_RED_PILOT =			0x05;
static const uint8_t SLOT_IR_PILOT = 			0x06;
static const uint8_t SLOT_GREEN_PILOT = 		0x07;

static const uint8_t MAX_30105_EXPECTEDPARTID = 0x15;

//The MAX30105 stores up to 32 samples on the IC
//This is additional local storage to the microcontroller
const int STORAGE_SIZE = 4; //Each long is 4 bytes so limit this to fit on your micro
//**********************************
//**********************************
//**********************************
void MAX30102Init(void)
{
	uint8_t trash[1];
	MAX3010xInitTypeDef MAX3010xInitStruct;
	
	MAX3010xInitStruct.LedMode = 2;
	MAX3010xInitStruct.RedLEDCurrent = 0x0f;
	MAX3010xInitStruct.IRLEDCurrent = 0x5f;
	MAX3010xInitStruct.GreenLEDCurrent = 0;
	MAX3010xInitStruct.ProximityModeLEDCurrent = 0;
	MAX3010xInitStruct.AveragedSamplesNum = 1;
	MAX3010xInitStruct.SampleRate = 1000;
	MAX3010xInitStruct.PulseWidth = 69;
	MAX3010xInitStruct.ADCRange = 16384;
	//MAX3010xInitStruct.FIFORollover = ENABLE;
	//MAX3010xInitStruct.;
	MAX3010xInitStruct.AFULLInterrupt = ENABLE;
	//MAX3010xInitStruct.DataReadyInterrupt = ENABLE;
	//MAX3010xInitStruct.ALCOverflowInterrupt;
	//MAX3010xInitStruct.FIFOAFullEmptySamples = 8;//32 - MAX3010xSamplesPacket;
	MAX3010xInitStruct.FIFOAFullFilledSamples = 24;//32 - MAX3010xSamplesPacket;
	MAX3010x_Init(&MAX3010xInitStruct);	
}
void MAX3010x_Init(MAX3010xInitTypeDef* MAX3010xInitStruct)
{
	
	//void setup(byte powerLevel, byte MAX3010xInitStruct->AveragedSampleNum, byte ledMode, int sampleRate, int pulseWidth, int adcRange) {
  softReset(); //Reset all configuration, threshold, and data registers to POR values

  //FIFO Configuration
  //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
  //The chip will average multiple samples of same type together if you wish
  if (MAX3010xInitStruct->AveragedSamplesNum == 1) setFIFOAverage(MAX30105_SAMPLEAVG_1); //No averaging per FIFO record
  else if (MAX3010xInitStruct->AveragedSamplesNum == 2) setFIFOAverage(MAX30105_SAMPLEAVG_2);
  else if (MAX3010xInitStruct->AveragedSamplesNum == 4) setFIFOAverage(MAX30105_SAMPLEAVG_4);
  else if (MAX3010xInitStruct->AveragedSamplesNum == 8) setFIFOAverage(MAX30105_SAMPLEAVG_8);
  else if (MAX3010xInitStruct->AveragedSamplesNum == 16) setFIFOAverage(MAX30105_SAMPLEAVG_16);
  else if (MAX3010xInitStruct->AveragedSamplesNum == 32) setFIFOAverage(MAX30105_SAMPLEAVG_32);
  else setFIFOAverage(MAX30105_SAMPLEAVG_4);

	if(MAX3010xInitStruct->DataReadyInterrupt == ENABLE)
	{
		enableDATARDY();
	}
  if(MAX3010xInitStruct->AFULLInterrupt == ENABLE)
	{
		if(MAX3010xInitStruct->FIFOAFullFilledSamples > MAX30105_FIFODEPTH) {
			MAX3010xInitStruct->FIFOAFullFilledSamples = MAX30105_FIFODEPTH;
		}
		setFIFOAlmostFull(MAX30105_FIFODEPTH - (MAX3010xInitStruct->FIFOAFullFilledSamples)); //Set to 30 samples to trigger an 'Almost Full' interrupt
		enableAFULL();
	}
	//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
	if (MAX3010xInitStruct->FIFORollover == ENABLE)
		enableFIFORollover(); //Allow FIFO to wrap/roll over
	else
		disableFIFORollover();
  
  //Mode Configuration
  //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
  if (MAX3010xInitStruct->LedMode == 3) setLEDMode(MAX30105_MODE_MULTILED); //Watch all three LED channels
  else if (MAX3010xInitStruct->LedMode == 2) setLEDMode(MAX30105_MODE_REDIRONLY); //Red and IR
  else setLEDMode(MAX30105_MODE_REDONLY); //Red only
  //activeLEDs = MAX3010xInitStruct->LedMode; //Used to control how many bytes to read from FIFO buffer

  //Particle Sensing Configuration
  //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
  if(MAX3010xInitStruct->ADCRange < 4096) setADCRange(MAX30105_ADCRANGE_2048); //7.81pA per LSB
  else if(MAX3010xInitStruct->ADCRange < 8192) setADCRange(MAX30105_ADCRANGE_4096); //15.63pA per LSB
  else if(MAX3010xInitStruct->ADCRange < 16384) setADCRange(MAX30105_ADCRANGE_8192); //31.25pA per LSB
  else if(MAX3010xInitStruct->ADCRange == 16384) setADCRange(MAX30105_ADCRANGE_16384); //62.5pA per LSB
  else setADCRange(MAX30105_ADCRANGE_2048);

  if (MAX3010xInitStruct->SampleRate < 100) setSampleRate(MAX30105_SAMPLERATE_50); //Take 50 samples per second
  else if (MAX3010xInitStruct->SampleRate < 200) setSampleRate(MAX30105_SAMPLERATE_100);
  else if (MAX3010xInitStruct->SampleRate < 400) setSampleRate(MAX30105_SAMPLERATE_200);
  else if (MAX3010xInitStruct->SampleRate < 800) setSampleRate(MAX30105_SAMPLERATE_400);
  else if (MAX3010xInitStruct->SampleRate < 1000) setSampleRate(MAX30105_SAMPLERATE_800);
  else if (MAX3010xInitStruct->SampleRate < 1600) setSampleRate(MAX30105_SAMPLERATE_1000);
  else if (MAX3010xInitStruct->SampleRate < 3200) setSampleRate(MAX30105_SAMPLERATE_1600);
  else if (MAX3010xInitStruct->SampleRate == 3200) setSampleRate(MAX30105_SAMPLERATE_3200);
  else setSampleRate(MAX30105_SAMPLERATE_50);

  //The longer the pulse width the longer range of detection you'll have
  //At 69us and 0.4mA it's about 2 inches
  //At 411us and 0.4mA it's about 6 inches
  if (MAX3010xInitStruct->PulseWidth < 118) setPulseWidth(MAX30105_PULSEWIDTH_69); //Page 26, Gets us 15 bit resolution
  else if (MAX3010xInitStruct->PulseWidth < 215) setPulseWidth(MAX30105_PULSEWIDTH_118); //16 bit resolution
  else if (MAX3010xInitStruct->PulseWidth < 411) setPulseWidth(MAX30105_PULSEWIDTH_215); //17 bit resolution
  else if (MAX3010xInitStruct->PulseWidth == 411) setPulseWidth(MAX30105_PULSEWIDTH_411); //18 bit resolution
  else setPulseWidth(MAX30105_PULSEWIDTH_69);
  //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

  //LED Pulse Amplitude Configuration
  //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
  //Default is 0x1F which gets us 6.4mA
  //powerLevel = 0x02, 0.4mA - Presence detection of ~4 inch
  //powerLevel = 0x1F, 6.4mA - Presence detection of ~8 inch
  //powerLevel = 0x7F, 25.4mA - Presence detection of ~8 inch
  //powerLevel = 0xFF, 50.0mA - Presence detection of ~12 inch

  
  
  //???????????????
  //setPulseAmplitudeProximity(MAX3010xInitStruct->ProximityModeLEDCurrent);
	
  //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

  //Multi-LED Mode Configuration, Enable the reading of the three LEDs
  //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
	setPulseAmplitudeRed(MAX3010xInitStruct->RedLEDCurrent);
  enableSlot(1, SLOT_RED_LED);
  if (MAX3010xInitStruct->LedMode > 1)
	{
		enableSlot(2, SLOT_IR_LED);
		setPulseAmplitudeIR(MAX3010xInitStruct->IRLEDCurrent);
	}
  if (MAX3010xInitStruct->LedMode > 2)
	{
		enableSlot(3, SLOT_GREEN_LED);
		setPulseAmplitudeGreen(MAX3010xInitStruct->GreenLEDCurrent);
	}
  //enableSlot(1, SLOT_RED_PILOT);
  //enableSlot(2, SLOT_IR_PILOT);
  //enableSlot(3, SLOT_GREEN_PILOT);
  //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

  clearFIFO(); //Reset the FIFO before we begin checking the sensor
}
void setReadPointer(uint8_t val)
{
	writeRegister8(MAX30105_FIFOREADPTR, val & MAX30105_FIFOREADPTR_MASK);
}
/*uint32_t MAX3010x_GetLastRedVal(void)
{
	setReadPointer(getWritePointer());
	return readRegister8sh(MAX30105_FIFODATA);
}*/
//**********************************
//**********************************
//**********************************
struct Record
{
  uint32_t red[STORAGE_SIZE];
  uint32_t IR[STORAGE_SIZE];
  uint32_t green[STORAGE_SIZE];
  uint8_t head;
  uint8_t tail;
} sense; //This is our circular buffer of readings from the sensor


/*
boolean begin(TwoWire &wirePort, uint32_t i2cSpeed, uint8_t i2caddr) {

  _i2cPort = &wirePort; //Grab which port the user wants us to use

  _i2cPort->begin();
  _i2cPort->setClock(i2cSpeed);

  _i2caddr = i2caddr;

  // Step 1: Initial Communciation and Verification
  // Check that a MAX30105 is connected
  if (!readPartID() == MAX_30105_EXPECTEDPARTID) {
    // Error -- Part ID read from MAX30105 does not match expected part ID.
    // This may mean there is a physical connectivity problem (broken wire, unpowered, etc).
    return false;
  }

  // Populate revision ID
  readRevisionID();

  return true;
}
*/
//
// Configuration
//
uint8_t getREG(uint8_t regNum) {
  return (readRegister8(regNum));
}
//Begin Interrupt configuration
uint8_t getINT1(void) {
  return (readRegister8(MAX30105_INTSTAT1));
}
uint8_t getINT2(void) {
  return (readRegister8(MAX30105_INTSTAT2));
}

void enableAFULL(void) {
  bitMask(MAX30105_INTENABLE1, MAX30105_INT_A_FULL_MASK, MAX30105_INT_A_FULL_ENABLE);
}
void disableAFULL(void) {
  bitMask(MAX30105_INTENABLE1, MAX30105_INT_A_FULL_MASK, MAX30105_INT_A_FULL_DISABLE);
}

void enableDATARDY(void) {
  bitMask(MAX30105_INTENABLE1, MAX30105_INT_DATA_RDY_MASK, MAX30105_INT_DATA_RDY_ENABLE);
}
void disableDATARDY(void) {
  bitMask(MAX30105_INTENABLE1, MAX30105_INT_DATA_RDY_MASK, MAX30105_INT_DATA_RDY_DISABLE);
}

void enableALCOVF(void) {
  bitMask(MAX30105_INTENABLE1, MAX30105_INT_ALC_OVF_MASK, MAX30105_INT_ALC_OVF_ENABLE);
}
void disableALCOVF(void) {
  bitMask(MAX30105_INTENABLE1, MAX30105_INT_ALC_OVF_MASK, MAX30105_INT_ALC_OVF_DISABLE);
}

void enablePROXINT(void) {
  bitMask(MAX30105_INTENABLE1, MAX30105_INT_PROX_INT_MASK, MAX30105_INT_PROX_INT_ENABLE);
}
void disablePROXINT(void) {
  bitMask(MAX30105_INTENABLE1, MAX30105_INT_PROX_INT_MASK, MAX30105_INT_PROX_INT_DISABLE);
}

void enableDIETEMPRDY(void) {
  bitMask(MAX30105_INTENABLE2, MAX30105_INT_DIE_TEMP_RDY_MASK, MAX30105_INT_DIE_TEMP_RDY_ENABLE);
}
void disableDIETEMPRDY(void) {
  bitMask(MAX30105_INTENABLE2, MAX30105_INT_DIE_TEMP_RDY_MASK, MAX30105_INT_DIE_TEMP_RDY_DISABLE);
}

//End Interrupt configuration

void softReset(void) {
  uint32_t i = 32000, j;
	
	bitMask(MAX30105_MODECONFIG, MAX30105_RESET_MASK, MAX30105_RESET);

  // Poll for bit to clear, reset is then complete
  // Timeout after 100ms
	
  
  while (i)
  {
    uint8_t response = readRegister8(MAX30105_MODECONFIG);
    if ((response & MAX30105_RESET) == 0) break; //We're done!
		i--;
    for(j=0;j<2000;j++); //Let's not over burden the I2C bus
  }
}

void shutDown(void) {
  // Put IC into low power mode (datasheet pg. 19)
  // During shutdown the IC will continue to respond to I2C commands but will
  // not update with or take new readings (such as temperature)
  bitMask(MAX30105_MODECONFIG, MAX30105_SHUTDOWN_MASK, MAX30105_SHUTDOWN);
}

void wakeUp(void) {
  // Pull IC out of low power mode (datasheet pg. 19)
  bitMask(MAX30105_MODECONFIG, MAX30105_SHUTDOWN_MASK, MAX30105_WAKEUP);
}

void setLEDMode(uint8_t mode) {
  // Set which LEDs are used for sampling -- Red only, RED+IR only, or custom.
  // See datasheet, page 19
  bitMask(MAX30105_MODECONFIG, MAX30105_MODE_MASK, mode);
}

void setADCRange(uint8_t adcRange) {
  // adcRange: one of MAX30105_ADCRANGE_2048, _4096, _8192, _16384
  bitMask(MAX30105_PARTICLECONFIG, MAX30105_ADCRANGE_MASK, adcRange);
}

void setSampleRate(uint8_t sampleRate) {
  // sampleRate: one of MAX30105_SAMPLERATE_50, _100, _200, _400, _800, _1000, _1600, _3200
  bitMask(MAX30105_PARTICLECONFIG, MAX30105_SAMPLERATE_MASK, sampleRate);
}

void setPulseWidth(uint8_t pulseWidth) {
  // pulseWidth: one of MAX30105_PULSEWIDTH_69, _188, _215, _411
  bitMask(MAX30105_PARTICLECONFIG, MAX30105_PULSEWIDTH_MASK, pulseWidth);
}

// NOTE: Amplitude values: 0x00 = 0mA, 0x7F = 25.4mA, 0xFF = 50mA (typical)
// See datasheet, page 21
void setPulseAmplitudeRed(uint8_t amplitude) {
  writeRegister8(MAX30105_LED1_PULSEAMP, amplitude);
}

void setPulseAmplitudeIR(uint8_t amplitude) {
  writeRegister8(MAX30105_LED2_PULSEAMP, amplitude);
}

void setPulseAmplitudeGreen(uint8_t amplitude) {
  writeRegister8(MAX30105_LED3_PULSEAMP, amplitude);
}

void setPulseAmplitudeProximity(uint8_t amplitude) {
  writeRegister8(MAX30105_LED_PROX_AMP, amplitude);
}

void setProximityThreshold(uint8_t threshMSB) {
  // Set the IR ADC count that will trigger the beginning of particle-sensing mode.
  // The threshMSB signifies only the 8 most significant-bits of the ADC count.
  // See datasheet, page 24.
  writeRegister8(MAX30105_PROXINTTHRESH, threshMSB);
}

//Given a slot number assign a thing to it
//Devices are SLOT_RED_LED or SLOT_RED_PILOT (proximity)
//Assigning a SLOT_RED_LED will pulse LED
//Assigning a SLOT_RED_PILOT will ??
void enableSlot(uint8_t slotNumber, uint8_t device) {

  //uint8_t originalContents;

  switch (slotNumber) {
    case (1):
      bitMask(MAX30105_MULTILEDCONFIG1, MAX30105_SLOT1_MASK, device);
      break;
    case (2):
      bitMask(MAX30105_MULTILEDCONFIG1, MAX30105_SLOT2_MASK, device << 4);
      break;
    case (3):
      bitMask(MAX30105_MULTILEDCONFIG2, MAX30105_SLOT3_MASK, device);
      break;
    case (4):
      bitMask(MAX30105_MULTILEDCONFIG2, MAX30105_SLOT4_MASK, device << 4);
      break;
    default:
      //Shouldn't be here!
      break;
  }
}

//Clears all slot assignments
void disableSlots(void) {
  writeRegister8(MAX30105_MULTILEDCONFIG1, 0);
  writeRegister8(MAX30105_MULTILEDCONFIG2, 0);
}

//
// FIFO Configuration
//

//Set sample average (Table 3, Page 18)
void setFIFOAverage(uint8_t numberOfSamples) {
  bitMask(MAX30105_FIFOCONFIG, MAX30105_SAMPLEAVG_MASK, numberOfSamples);
}

//Resets all points to start in a known state
//Page 15 recommends clearing FIFO before beginning a read
void clearFIFO(void) {
  writeRegister8(MAX30105_FIFOWRITEPTR, 0);
  writeRegister8(MAX30105_FIFOOVERFLOW, 0);
  writeRegister8(MAX30105_FIFOREADPTR, 0);
}

//Enable roll over if FIFO over flows
void enableFIFORollover(void) {
  bitMask(MAX30105_FIFOCONFIG, MAX30105_ROLLOVER_MASK, MAX30105_ROLLOVER_ENABLE);
}

//Disable roll over if FIFO over flows
void disableFIFORollover(void) {
  bitMask(MAX30105_FIFOCONFIG, MAX30105_ROLLOVER_MASK, MAX30105_ROLLOVER_DISABLE);
}

//Set number of samples to trigger the almost full interrupt (Page 18)
//Power on default is 32 samples
//Note it is reverse: 0x00 is 32 samples, 0x0F is 17 samples
void setFIFOAlmostFull(uint8_t numberOfSamples) {
  bitMask(MAX30105_FIFOCONFIG, MAX30105_A_FULL_MASK, numberOfSamples);
}

//Read the FIFO Write Pointer
uint8_t getWritePointer(void) {
  return (readRegister8(MAX30105_FIFOWRITEPTR));
}

//Read the FIFO Read Pointer
uint8_t getReadPointer(void) {
  return (readRegister8(MAX30105_FIFOREADPTR));
}
// Die Temperature
// Returns temp in C
float readTemperature() {
	int8_t tempInt;
	uint8_t tempFrac;
	uint32_t i=32000, j;
  // Step 1: Config die temperature register to take 1 temperature sample
  writeRegister8(MAX30105_DIETEMPCONFIG, 0x01);

  // Poll for bit to clear, reading is then complete
  // Timeout after 100ms
  
	while (i)
  {
    uint8_t response = readRegister8(MAX30105_DIETEMPCONFIG);
    if ((response & 0x01) == 0) break; //We're done!
		i--;
    for(j=0;j<2000;j++); //Let's not over burden the I2C bus
  }
  //TODO How do we want to fail? With what type of error?
  //? if(millis() - startTime >= 100) return(-999.0);

  // Step 2: Read die temperature register (integer)
  tempInt = readRegister8(MAX30105_DIETEMPINT);
  tempFrac = readRegister8(MAX30105_DIETEMPFRAC);

  // Step 3: Calculate temperature (datasheet pg. 23)
  return (float)tempInt + ((float)tempFrac * 0.0625);
}

// Returns die temp in F
float readTemperatureF() {
  float temp = readTemperature();

  if (temp != -999.0) temp = temp * 1.8 + 32.0;

  return (temp);
}

// Set the PROX_INT_THRESHold
void setPROXINTTHRESH(uint8_t val) {
  writeRegister8(MAX30105_PROXINTTHRESH, val);
}


//
// Device ID and Revision
//
uint8_t readPartID() {
  return readRegister8(MAX30105_PARTID);
}

/*void readRevisionID() {
  revisionID = readRegister8(_i2caddr, MAX30105_REVISIONID);
}

uint8_t getRevisionID() {
  return revisionID;
}
*/

//Setup the sensor
//The MAX30105 has many settings. By default we select:
// Sample Average = 4
// Mode = MultiLED
// ADC Range = 16384 (62.5pA per LSB)
// Sample rate = 50
//Use the default setup if you are just getting started with the MAX30105 sensor

//
// Data Collection
//

//Tell caller how many samples are available
/*uint8_t available(void)
{
  uint8_t numberOfSamples = sense.head - sense.tail;
  if (numberOfSamples < 0) numberOfSamples += STORAGE_SIZE;

  return (numberOfSamples);
}*/
/*
//Report the most recent red value
uint32_t getRed(void)
{
  //Check the sensor for new data for 250ms
  if(safeCheck(250))
    return (sense.red[sense.head]);
  else
    return(0); //Sensor failed to find new data
}

//Report the most recent IR value
uint32_t getIR(void)
{
  //Check the sensor for new data for 250ms
  if(safeCheck(250))
    return (sense.IR[sense.head]);
  else
    return(0); //Sensor failed to find new data
}

//Report the most recent Green value
uint32_t getGreen(void)
{
  //Check the sensor for new data for 250ms
  if(safeCheck(250))
    return (sense.green[sense.head]);
  else
    return(0); //Sensor failed to find new data
}
*/
//Report the next Red value in the FIFO
uint32_t getFIFORed(void)
{
  return (sense.red[sense.tail]);
}

//Report the next IR value in the FIFO
uint32_t getFIFOIR(void)
{
  return (sense.IR[sense.tail]);
}

//Report the next Green value in the FIFO
uint32_t getFIFOGreen(void)
{
  return (sense.green[sense.tail]);
}
//Given a register, read it, mask it, and then set the thing
void bitMask(uint8_t reg, uint8_t mask, uint8_t thing)
{
	
  // Grab current register context
  uint8_t originalContents;
	
	originalContents = readRegister8(reg);
	
  // Zero-out the portions of the register we're interested in
  originalContents = originalContents & mask;

  // Change contents
  writeRegister8(reg, originalContents | thing);
}
//**********************************
//**********************************
//**********************************
uint8_t readRegister8(uint8_t reg)
{
	uint8_t addr = reg;
  uint8_t data = 0;
  uint8_t d;
  while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY);
  d = HAL_I2C_Master_Transmit(&hi2c1, MAX30105_ADDRESS<<1, &addr, 1, 1000);
  if ( d != HAL_OK) {
      return d;
  }

  while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY);
  d = HAL_I2C_Master_Receive(&hi2c1, MAX30105_ADDRESS<<1, &data, 1, 1000);
  if ( d != HAL_OK) {
      return d;
	}
  return data;
}
void writeRegister8(uint8_t reg, uint8_t value)
{
  uint8_t buf[] = {reg, value};
  uint8_t d;
  while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY);
  d = HAL_I2C_Master_Transmit(&hi2c1, MAX30105_ADDRESS<<1, buf, 2, 1000);
}
uint32_t readRegister8sh(uint8_t reg)
{
  uint8_t pData[3];
  uint8_t d;
  while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY);
  d = HAL_I2C_Master_Transmit(&hi2c1, MAX30105_ADDRESS<<1, &reg, 1, 1000);
  if ( d != HAL_OK) {
      return d;
  }

  while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY);
  d = HAL_I2C_Master_Receive(&hi2c1, MAX30105_ADDRESS<<1, &pData[0], 3, 1000);
  if ( d != HAL_OK) {
      return d;
	}
	return (((pData[0])<<16)|(pData[1]<<8)|(pData[2]));
}
uint8_t MAX3010xFIFORead()
{
	uint8_t reg = 0x07;
	HAL_I2C_Master_Transmit_IT(&hi2c1, MAX30105_ADDRESS<<1, &reg, 1);
}

//**********************************
//**********************************
//**********************************
